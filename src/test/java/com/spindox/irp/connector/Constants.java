package com.spindox.irp.connector;

public final class Constants {
	public static final String QUERY_FINDPROJECT_FOO = "projects/index?key=fooKey";
	public static final String QUERY_COMPONENTLIST_TRK = "components/search?qualifiers=TRK";
	public static final String QUERY_TOTALBUGS = "measures/component?componentKey=fooKey&metricKeys=bugs";
	public static final String QUERY_TOTALVULNERABILITIES = "measures/component?componentKey=fooKey&metricKeys=vulnerabilities";
	public static final String QUERY_TOTALCODESMELLS = "measures/component?componentKey=fooKey&metricKeys=code_smells";
	public static final String QUERY_TOTALVIOLATIONS = "measures/component?componentKey=fooKey&metricKeys=violations";
	public static final String QUERY_MEASURES_BY_COMPONENT = "measures/component?componentKey=fooKey&metricKeys=fooKey";
	public static final String QUERY_NCLOC_BYCOMPONENT = "measures/component?componentKey=fooKey&metricKeys=ncloc";
	public static final String QUERY_BUG_BREAKDOWN = "issues/search?componentKeys=fooKey&facets=severities,types&facetMode=count&resolved=false&types=BUG";
	public static final String QUERY_VULNERABILITY_BREAKDOWN = "issues/search?componentKeys=fooKey&facets=severities,types&facetMode=count&resolved=false&types=VULNERABILITY";
	public static final String QUERY_CODE_SMELL_BREAKDOWN = "issues/search?componentKeys=fooKey&facets=severities,types&facetMode=count&resolved=false&types=CODE_SMELL";
	protected static final String[] TYPES = { "BUG", "VULNERABILITY", "CODE_SMELL" };
	protected static final String[] SEVERITIES = { "BLOCKER", "CRITICAL", "MAJOR", "MINOR", "INFO" };
	protected static final String[] ISSUE_TYPE_LIST = { "component_name", "component_qualifier", "type", "loc",
			"bug_blocker", "bug_critical", "bug_major", "bug_minor", "bug_info", "vulnerability_blocker",
			"vulnerability_critical", "vulnerability_major", "vulnerability_minor", "vulnerability_info",
			"maintanibility_blocker", "maintanibility_critical", "maintanibility_major", "maintanibility_minor",
			"maintanibility_info" };


	private Constants() {

	}

}
