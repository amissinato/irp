package com.spindox.irp.connector;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import org.apache.commons.io.Charsets;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;

import com.github.jknack.handlebars.internal.Files;
import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.ResponseDefinitionBuilder;
import com.github.tomakehurst.wiremock.http.HttpHeader;
import com.github.tomakehurst.wiremock.http.HttpHeaders;
import com.spindox.irp.connector.sonar.SonarConnector;

import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;
import static com.github.tomakehurst.wiremock.client.WireMock.*;

class SonarConnectorTest {

	private WireMockServer sonarMockServer;
	private static final String FOO_TOKEN = "fooToken";
	private static URL FOO_URL = null;
	private static String BASE_URL = null;
	private static String FOO_PATH = null;
	private static final String FOO_QUERYAPI = "fooParam?key=foo";
	private static HttpHeaders HTTP_HEADERS = new HttpHeaders(new HttpHeader("Accept", "application/json"),
			new HttpHeader("Accept-Charset", "UTF-8"));

	@BeforeEach
	void setUp() throws Exception {
		this.sonarMockServer = new WireMockServer(
				(options().bindAddress("127.0.0.1").dynamicPort()));
		this.sonarMockServer.start();
		configureFor(this.sonarMockServer.port());
		String port = String.valueOf(sonarMockServer.port());
		BASE_URL = String.format("http://localhost:%s/", port);
		FOO_PATH = String.format("%s/fooPath/%s", BASE_URL, FOO_QUERYAPI);
		FOO_URL = new URL(FOO_PATH);

	}

	@AfterEach
	void tearDown() throws Exception {
		this.sonarMockServer.stop();

	}

	/**
	 *
	 *
	 * @param queryPath es: api/projects/index?key=fooKey
	 * @param params    an array of "key=value" param strings
	 * @return the URL string, es:
	 *         http://127.0.0.1:8080/api/projects/index?key=fooKey
	 */
	private String buildApiUrl(String queryPath, String[] params) {
		StringBuilder sb = new StringBuilder(queryPath).append("?");
		for (String param : params) {
			sb.append(param);
		}
		return BASE_URL.toString().concat(sb.toString());
	}

	/**
	 *
	 * @param fileName
	 * @return
	 * @throws IOException
	 */
	private String getResourceFileContent(String fileName) throws IOException {
		InputStream is = getClass().getResourceAsStream(String.format("/__files/%s", fileName));
		return Files.read(is, Charsets.UTF_8);
	}

	@Test
	void testSonarConnector() throws MalformedURLException {
		String expectedBaseURL = String.format("%s/fooPath", BASE_URL);
		SonarConnector sc = new SonarConnector(FOO_URL, FOO_TOKEN);
		assertEquals(FOO_URL, sc.getServiceURL());
		assertEquals(expectedBaseURL, sc.getBaseURL());
		assertEquals(FOO_QUERYAPI, sc.getQueryAPI());
		assertEquals(FOO_TOKEN, sc.getToken());

	}

	@Test
	void testSonarConnectorWithNullURL() {
		// an exception must be thrown if the url is null
		assertThrows(MalformedURLException.class, () -> new SonarConnector(null, FOO_TOKEN));

	}

	@Test
	@DisplayName("Should return the project info given the component Key")
	void testGetResponseFindProjectByKey() throws IOException {
		String urlPath = "api/projects/index";
		String[] params = { "key=fooKey" };
		String responseFileName = "projectByKey.json";
		ResponseDefinitionBuilder rdb = ok().withBodyFile(responseFileName);
		givenThat(get(urlPathMatching("/" + urlPath)).willReturn(rdb.withHeaders(HTTP_HEADERS)));
		SonarConnector sc = new SonarConnector(new URL(buildApiUrl(urlPath, params)), FOO_TOKEN);
		String expected = getResourceFileContent(responseFileName);
		String resp = sc.getResponseContent();
		// compare the two json regardless of the field order
		JSONAssert.assertEquals(expected, resp, JSONCompareMode.NON_EXTENSIBLE);
	}

	@Test
	void testGetResponseGetComponentsListByQualifier() throws IOException {
		String urlPath = "api/components/search";
		String[] params = { "qualifiers=TRK" };
		String responseFileName = "componentsByQualifier.json";
		ResponseDefinitionBuilder rdb = ok().withBodyFile(responseFileName);
		givenThat(get(urlPathMatching("/" + urlPath)).willReturn(rdb.withHeaders(HTTP_HEADERS)));
		SonarConnector sc = new SonarConnector(new URL(buildApiUrl(urlPath, params)), FOO_TOKEN);
		String expected = getResourceFileContent(responseFileName);
		String resp = sc.getResponseContent();
		// compare the two json regardless of the field order
		JSONAssert.assertEquals(expected, resp, JSONCompareMode.NON_EXTENSIBLE);
		// "components/search?qualifiers=TRK";

	}

}
