package com.spindox.irp.service;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;
import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.Charsets;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;

import com.github.jknack.handlebars.internal.Files;
import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.MappingBuilder;
import com.github.tomakehurst.wiremock.client.ResponseDefinitionBuilder;
import com.github.tomakehurst.wiremock.http.HttpHeader;
import com.github.tomakehurst.wiremock.http.HttpHeaders;
import com.github.tomakehurst.wiremock.matching.StringValuePattern;
import com.github.tomakehurst.wiremock.stubbing.StubMapping;
import com.spindox.irp.model.IComponent;
import com.spindox.irp.model.sonar.SonarProject;
import static com.spindox.irp.model.sonar.SonarConstants.*;

class SonarReportTest {
	private WireMockServer sonarMockServer;
	private static final String FOO_TOKEN = "fooToken";
	private static String BASE_URL = null;
	private static String FOO_PATH = null;
	private static final String FOO_QUERYAPI = "fooParam?key=foo";
	private static final String PROJECT_QUALIFIER = "TRK";
	private static HttpHeaders HTTP_HEADERS = new HttpHeaders(new HttpHeader("Accept", "application/json"),
			new HttpHeader("Accept-Charset", "UTF-8"));

	@BeforeEach
	void setUp() throws Exception {
		this.sonarMockServer = new WireMockServer((options()
				//.bindAddress("127.0.0.1")
				.dynamicPort()
				//.dynamicHttpsPort()
				//.port(9000)
				//.httpsPort(9100)
				//.proxyVia("127.0.0.1", 8080)
				));
		this.sonarMockServer.start();
		configureFor(this.sonarMockServer.port());
		String port = String.valueOf(sonarMockServer.port());
		BASE_URL = String.format("http://localhost:%s", port);
		FOO_PATH = String.format("%s/fooPath/%s", BASE_URL, FOO_QUERYAPI);
		new URL(FOO_PATH);

	}

	@AfterEach
	void tearDown() throws Exception {
		this.sonarMockServer.stop();

	}

	/**
	 *
	 * @param fileName
	 * @return
	 * @throws IOException
	 */
	private String getResourceFileContent(String fileName) throws IOException {
		InputStream is = getClass().getResourceAsStream(String.format("/__files/%s", fileName));
		return Files.read(is, Charsets.UTF_8);
	}


	/**
	 *
	 * @param fileName
	 * @return
	 * @throws IOException
	 * @throws JSONException
	 */
	private JSONObject readJSONObject(String fileName) throws JSONException, IOException {
		return new JSONObject(getResourceFileContent(fileName));
	}

	private JSONArray readJSONArray(String fileName) throws JSONException, IOException{
		return new JSONArray(getResourceFileContent(fileName));
	}


	/**
	 *
	 * @param urlPath
	 * @param qualifier
	 * @param responseFileName
	 * @param rdb
	 * @param token
	 * @param headers
	 * @return
	 */
	private StubMapping buildServiceMock(String urlPath, String qualifier
			, String responseFileName, ResponseDefinitionBuilder rdb
			, String token, HttpHeaders headers, Map<String, StringValuePattern> params) {
		String path = (urlPath.charAt(0) == '/') ? urlPath : "/".concat(urlPath);
		MappingBuilder mappingBuilder = get(urlPathMatching(path))
				.withBasicAuth(token,"")
				.withQueryParams(params)
				.willReturn(rdb.withHeaders(headers));
		return givenThat(mappingBuilder);
	}

	@Test
	void testGetProjectByKey() throws IOException {
		String server = String.format("%s/", BASE_URL);
		String urlPath = "api/projects/index";
		String responseFileName = "projectByKey.json";
		ResponseDefinitionBuilder rdb = ok().withBodyFile(responseFileName);
		Map<String, StringValuePattern> params = new HashMap<String, StringValuePattern>();
		params.put("key",equalTo("fooKey"));
		buildServiceMock(urlPath, PROJECT_QUALIFIER, responseFileName, rdb, FOO_TOKEN, HTTP_HEADERS, params);
		JSONObject obj = readJSONArray(responseFileName).getJSONObject(0);
		String key = obj.getString(PARAM_COMPONENTKEY);
		IComponent expected = new SonarProject(
				null, String.valueOf(obj.get(PARAM_COMPONENTID)), obj.getString(PARAM_COMPONENTKEY)
				, obj.getString(PARAM_COMPONENTNAME), obj.getString(PARAM_COMPONENTQUALIFIER)
				, obj.getString(PARAM_COMPONENTTYPE), null);
		IComponent actual = SonarReport.getProjectByKey(server, key, FOO_TOKEN);
		assertEquals(expected, actual);
	}

	@Test
	void testGetProjectList() throws IOException {
		String server = String.format("%s/", BASE_URL);
		String urlPath = "api/components/search";
		String responseFileName = "componentsByQualifier.json";
		ResponseDefinitionBuilder rdb = ok().withBodyFile(responseFileName);
		Map<String, StringValuePattern> params = new HashMap<String, StringValuePattern>();
		params.put("qualifiers",equalTo(PROJECT_QUALIFIER));
		buildServiceMock(urlPath, PROJECT_QUALIFIER, responseFileName, rdb, FOO_TOKEN, HTTP_HEADERS, params);
		JSONObject responseObj = readJSONObject(responseFileName);
		org.json.JSONArray expected = responseObj.getJSONArray("components");
		org.json.JSONArray actual = SonarReport.getProjectList(server, FOO_TOKEN, PROJECT_QUALIFIER);
		JSONAssert.assertEquals(expected, actual, JSONCompareMode.NON_EXTENSIBLE);
	}

	@Test
	void testGetProjectKeys() throws JSONException, IOException {
		String server = String.format("%s/", BASE_URL);
		String urlPath = "api/components/search";
		String responseFileName = "componentsByQualifier.json";
		ResponseDefinitionBuilder rdb = ok().withBodyFile(responseFileName);
		Map<String, StringValuePattern> params = new HashMap<String, StringValuePattern>();
		params.put("qualifiers",equalTo(PROJECT_QUALIFIER));
		buildServiceMock(urlPath, PROJECT_QUALIFIER, responseFileName, rdb, FOO_TOKEN, HTTP_HEADERS, params);
		org.json.JSONArray expected = new org.json.JSONArray();
		org.json.JSONArray components = readJSONObject(responseFileName).getJSONArray("components");
		components.forEach(comp -> {
			String key = ((JSONObject)comp).getString("key");
			expected.put(new JSONObject().put("key", key));
		});
		org.json.JSONArray actual = SonarReport.getProjectKeys(server, FOO_TOKEN, PROJECT_QUALIFIER);
		JSONAssert.assertEquals(expected, actual, JSONCompareMode.NON_EXTENSIBLE);
	}


}
