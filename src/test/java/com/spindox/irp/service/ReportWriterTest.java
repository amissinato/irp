package com.spindox.irp.service;

import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.FileSystem;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;

import com.google.common.jimfs.Configuration;
import com.google.common.jimfs.Jimfs;
import com.opencsv.bean.ColumnPositionMappingStrategy;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.enums.CSVReaderNullFieldIndicator;
import com.spindox.irp.model.IComponent;
import com.spindox.irp.model.sonar.SonarMetricBean;
import com.spindox.irp.model.sonar.SonarProject;
import com.spindox.irp.report.sonar.SonarIssueType;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;

class ReportWriterTest {
	static FileSystem fs = null;
	static Path fooPath = null;
	static final String TEMP_DIR = "/tmp";
	static List<SonarMetricBean> contentLines = null;
	static Path report = null;

	@BeforeAll
	static void createFileSystem() throws IOException {
		fs = Jimfs.newFileSystem(Configuration.unix());
		fooPath = fs.getPath(TEMP_DIR);
		Files.createDirectory(fooPath);
		report = fooPath.resolve("report.csv");

	}

	private static SonarMetricBean buildMetric(String key, String qualifier, String name) {
		IComponent prj = new SonarProject(key, qualifier, name);
		SonarIssueType bugs = buildSonarIssue("BUG", 15, 27, 99, 66, 199);
		SonarIssueType vulnerabilities = buildSonarIssue("VULNERABILITY", 7, 44, 12, 243, 111);
		SonarIssueType codeSmells = buildSonarIssue("CODE_SMELL", 123, 77, 343, 166, 2715);
		return new SonarMetricBean(prj, bugs, vulnerabilities, codeSmells);
	}

	private static SonarIssueType buildSonarIssue(String type, int blockerIssues, int criticalIssues, int majorIssues
			, int minorIssues, int infoIssues) {
		SonarIssueType result = new SonarIssueType.Builder(type).withBlockerIssues(blockerIssues)
				.withCriticalIssues(criticalIssues).withMajorIssues(majorIssues).withMinorIssues(minorIssues)
				.withInfoIssues(infoIssues).build();
		return result;
	}

	private JSONArray toJSONArray(List<SonarMetricBean> lines) {
		JSONArray result = new JSONArray();
		lines.forEach(bean -> {
			JSONObject jo = bean.toJson();
			result.put(jo);
		});
		return result;
	}

	/**
	 *
	 * @param path
	 * @param clazz
	 * @return
	 * @throws IOException
	 */
	private static List<SonarMetricBean> readCSV(Path path, Class<SonarMetricBean> clazz) throws IOException {
		List<SonarMetricBean> resultList = new ArrayList<>();
	     ColumnPositionMappingStrategy<SonarMetricBean> ms = new ColumnPositionMappingStrategy<SonarMetricBean>();
	     ms.setType(clazz);
	     Reader reader = Files.newBufferedReader(path);
	     CsvToBean<SonarMetricBean> cb = new CsvToBeanBuilder<SonarMetricBean>(reader)
	       .withType(clazz)
	       .withMappingStrategy(ms)
	       .withSkipLines(1)
	       .withSeparator(';')
	       .withFieldAsNull(CSVReaderNullFieldIndicator.EMPTY_QUOTES)
	       .build();
	    resultList = cb.parse();
	    reader.close();
	    return resultList;
	}

	@AfterAll
	static void closeFileSystem() throws Exception {
		fs.close();
	}

	@Test
	final void testWrite() throws Exception {
		contentLines = new ArrayList<>();
		contentLines.add(buildMetric("fooKey1", "TRK", "fooName1"));
		contentLines.add(buildMetric("fooKey2", "TRK", "fooName17"));
		File reportFile = File.createTempFile(fooPath.toString(), "");
		ReportWriter.write(contentLines, reportFile);
		JSONArray expected = toJSONArray(contentLines);
		List<SonarMetricBean> actualLines = readCSV(reportFile.toPath(), SonarMetricBean.class);
		JSONArray actual = toJSONArray(actualLines);
		JSONAssert.assertEquals(expected, actual, JSONCompareMode.NON_EXTENSIBLE);
	}


}
