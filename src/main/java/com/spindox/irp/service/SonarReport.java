package com.spindox.irp.service;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.PathNotFoundException;
import com.spindox.irp.connector.sonar.SonarConnector;
import com.spindox.irp.model.IComponent;
import com.spindox.irp.model.IMeasure;
import com.spindox.irp.model.sonar.SonarMeasure;
import com.spindox.irp.model.sonar.SonarMetricBean;
import com.spindox.irp.model.sonar.SonarProject;
import com.spindox.irp.report.sonar.SonarIssueType;
import static com.spindox.irp.model.sonar.SonarConstants.*;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class SonarReport {
	private static final Logger logger = LoggerFactory.getLogger(SonarReport.class);
	public static final String QUERY_FINDPROJECT = "api/projects/index?key=%s";
	public static final String QUERY_COMPONENTLIST = "api/components/search?qualifiers=%s";
	public static final String QUERY_TOTALBUGS = "api/measures/component?componentKey=%s&metricKeys=bugs";
	public static final String QUERY_TOTALVULNERABILITIES = "api/measures/component?componentKey=%s&metricKeys=vulnerabilities";
	public static final String QUERY_TOTALCODESMELLS = "api/measures/component?componentKey=%s&metricKeys=code_smells";
	public static final String QUERY_TOTALVIOLATIONS = "api/measures/component?componentKey=%s&metricKeys=violations";
	public static final String QUERY_MEASURES_BY_COMPONENT = "api/measures/component?componentKey=%s&metricKeys=%s";
	public static final String QUERY_NCLOC_BYCOMPONENT = "api/measures/component?componentKey=%s&metricKeys=ncloc";
	public static final String QUERY_MEASURES_BREAKDOWN = "api/issues/search?componentKeys=%s&facets=severities,types&facetMode=count&resolved=false&types=%s";
	protected static final String[] TYPES = { "BUG", "VULNERABILITY", "CODE_SMELL" };
	protected static final String[] SEVERITIES = { "BLOCKER", "CRITICAL", "MAJOR", "MINOR", "INFO" };
	protected static final String[] ISSUE_TYPE_LIST = { "component_name", "component_qualifier", "type", "loc",
			"bug_blocker", "bug_critical", "bug_major", "bug_minor", "bug_info", "vulnerability_blocker",
			"vulnerability_critical", "vulnerability_major", "vulnerability_minor", "vulnerability_info",
			"maintanibility_blocker", "maintanibility_critical", "maintanibility_major", "maintanibility_minor",
			"maintanibility_info" };
	private static final String METRIC_KEY = "metric";
	private static final String VALUE_KEY = "value";

	public  enum TYPE {
		BUG, VULNERABILITY, CODE_SMELL
	}

	public  enum SEVERITY {
		BLOCKER, CRITICAL, MAJOR, MINOR, INFO
	}

	private SonarReport() {
	}

	private static URL buildURL(String serverURL, String path) throws MalformedURLException{
		String url = new StringBuilder().append(serverURL).append(path).toString();
		return new URL(url);
	}
	/**
	 *
	 * @param baseURL
	 * @param key
	 * @param token
	 * @return
	 * @throws MalformedURLException
	 */
	public static IComponent getProjectByKey(String baseURL, String key, String token) throws MalformedURLException {
		String urlPath = String.format(QUERY_FINDPROJECT, key);
		URL url = buildURL(baseURL, urlPath);
		SonarConnector sc = new SonarConnector(url, token);
		String response = sc.getResponseContent();
		JSONArray resp = new JSONArray(response);
		JSONObject prj = resp.getJSONObject(0);
		String organization = null; //unused for now
		String id = String.valueOf(prj.getInt(PARAM_COMPONENTID));
		String name = prj.getString(PARAM_COMPONENTNAME);
		String qualifier = prj.getString(PARAM_COMPONENTQUALIFIER);
		String type = prj.getString(PARAM_COMPONENTTYPE);
		Integer linesOfCode = null;
		return new SonarProject(organization, id, key, name, qualifier, type, linesOfCode);
	}

	/**
	 *
	 * @param baseURL es: http://codereview.edison.it/
	 * @param token
	 * @return
	 * @throws MalformedURLException
	 */
	public static JSONArray getProjectList(String baseURL, String token, String qualifier)
			throws MalformedURLException {
		String urlPath = String.format(QUERY_COMPONENTLIST, qualifier);
		URL url = buildURL(baseURL, urlPath);
		SonarConnector sc = new SonarConnector(url, token);
		String response = sc.getResponseContent();
		JSONObject jo = new JSONObject(response);
		return jo.getJSONArray("components");
	}

	/**
	 *
	 * @param baseURL
	 * @param token
	 * @return
	 * @throws MalformedURLException
	 */
	public static JSONArray getProjectKeys(String baseURL, String token, String qualifier)
			throws MalformedURLException {
		JSONArray result = new JSONArray();
		JSONArray projects = getProjectList(baseURL, token, qualifier);
		projects.forEach(item -> {
			JSONObject obj = (JSONObject) item;
			result.put(new JSONObject().put("key", obj.get("key")));
		});
		return result;
	}

	/**
	 * 
	 * @param baseURL
	 * @param token
	 * @param qualifier
	 * @return
	 * @throws MalformedURLException
	 */
	public static List<String> getProjectKeyList(String baseURL, String token, String qualifier)
			throws MalformedURLException {
		List<String> keys = new ArrayList<>();
		JSONArray jkeys = getProjectKeys(baseURL, token, qualifier);
		jkeys.forEach(item -> {
			JSONObject jk = (JSONObject) item;
			keys.add(jk.getString("key"));
		});
		return keys;
	}

	/**
	 * 
	 * @param queryString
	 * @param baseURL
	 * @param key
	 * @param token
	 * @return
	 * @throws MalformedURLException
	 */
	public static IMeasure getMeasureByComponent(String queryString, String baseURL, String key, String token)
			throws MalformedURLException {
		String query = String.format(queryString, key);
		URL url = buildURL(baseURL, query);
		SonarConnector sc = new SonarConnector(url, token);
		String response = sc.getResponseContent();
		Map<String, Object> map = null;
		try {
			map = JsonPath.parse(response).read("component.measures[0]");
		} catch (PathNotFoundException e) {
			logger.debug(e.getMessage(), e);
		}
		if (map == null) {
			return new SonarMeasure("N/A", 0);
		}
		JSONObject ncloc = new JSONObject(map);
		return new SonarMeasure(ncloc.getString(METRIC_KEY), ncloc.getInt(VALUE_KEY));
	}

	/**
	 *
	 * @param baseURL
	 * @param key
	 * @param token
	 * @return
	 * @throws MalformedURLException
	 */
	public static IMeasure getLinesOfCodeByComponent(String baseURL, String key, String token)
			throws MalformedURLException {
		return getMeasureByComponent(QUERY_NCLOC_BYCOMPONENT, baseURL, key, token);
	}

	/**
	 *
	 * @param baseURL
	 * @param key
	 * @param token
	 * @return
	 * @throws MalformedURLException
	 */
	public static IMeasure getTotalViolationsByComponent(String baseURL, String key, String token)
			throws MalformedURLException {
		return getMeasureByComponent(QUERY_TOTALVIOLATIONS, baseURL, key, token);
	}

	/**
	 *
	 * @param baseURL
	 * @param key
	 * @param token
	 * @return
	 * @throws MalformedURLException
	 */
	public static IMeasure getTotalBugsByComponent(String baseURL, String key, String token)
			throws MalformedURLException {
		return getMeasureByComponent(QUERY_TOTALBUGS, baseURL, key, token);
	}

	/**
	 *
	 * @param baseURL
	 * @param key
	 * @param token
	 * @return
	 * @throws MalformedURLException
	 */
	public static IMeasure getTotalCodeSmellsByComponent(String baseURL, String key, String token)
			throws MalformedURLException {
		return getMeasureByComponent(QUERY_TOTALCODESMELLS, baseURL, key, token);
	}

	/**
	 *
	 * @param baseURL
	 * @param key
	 * @param token
	 * @return
	 * @throws MalformedURLException
	 */
	public static IMeasure getTotalVulnerabilitiesByComponent(String baseURL, String key, String token)
			throws MalformedURLException {
		String query = String.format(QUERY_TOTALVULNERABILITIES, key);
		String url = new StringBuilder().append(baseURL).append(query).toString();
		SonarConnector sc = new SonarConnector(new URL(url), token);
		String response = sc.getResponseContent();
		Map<String, Object> map = JsonPath.parse(response).read("component.measures[0]");
		JSONObject metric = new JSONObject(map);
		return new SonarMeasure(metric.getString(METRIC_KEY), metric.getInt(VALUE_KEY));
	}

	/**
	 *
	 * @param baseURL
	 * @param key
	 * @param token
	 * @return
	 * @throws MalformedURLException
	 */
	public static JSONArray getMeasureBreakdownByTypeAndComponent(String baseURL, String key, String token,
			String param) throws MalformedURLException {
		String query = String.format(QUERY_MEASURES_BREAKDOWN, key, param);
		String url = new StringBuilder().append(baseURL).append(query).toString();
		SonarConnector sc = new SonarConnector(new URL(url), token);
		String response = sc.getResponseContent();
		List<Map<String, Object>> list = JsonPath.parse(response).read("$.facets[?(@.property=='severities')].values");
		JSONArray result = new JSONArray();
		JSONArray measures = new JSONArray(list).getJSONArray(0);
		measures.forEach(measure -> {
			JSONObject value = ((JSONObject) measure);
			SonarMeasure sm = new SonarMeasure(value.getString("val"), value.getInt("count"));
			result.put(sm.toJson());

		});

		return result;
	}

	/**
	 * 
	 * @param baseURL
	 * @param key
	 * @param token
	 * @param type
	 * @return
	 * @throws MalformedURLException
	 */
	private static List<SonarMeasure> getMeasuresByTypeAndComponent(String baseURL, String key, String token,
			String type) throws MalformedURLException {
		List<SonarMeasure> result = new ArrayList<>();
		String query = String.format(QUERY_MEASURES_BREAKDOWN, key, type);
		String url = new StringBuilder().append(baseURL).append(query).toString();
		SonarConnector sc = new SonarConnector(new URL(url), token);
		String response = sc.getResponseContent();
		List<Map<String, Object>> list = JsonPath.parse(response).read("$.facets[?(@.property=='severities')].values");
		JSONArray measures = new JSONArray(list).getJSONArray(0);
		measures.forEach(measure -> {
			JSONObject value = ((JSONObject) measure);
			SonarMeasure sm = new SonarMeasure(value.getString("val"), value.getInt("count"));
			result.add(sm);

		});

		return result;

	}

	/**
	 * 
	 * @param baseURL
	 * @param key
	 * @param token
	 * @return
	 * @throws MalformedURLException
	 */
	public static SonarMetricBean getReportEntry(String baseURL, String key, String token)
			throws MalformedURLException {
		SonarMetricBean bean = null;
		IComponent prj = getProjectByKey(baseURL, key, token);
		IMeasure linesOfCode = getLinesOfCodeByComponent(baseURL, key, token);
		String id = prj.getId();
		prj.setId(id);
		prj.setLinesOfCode(linesOfCode.getValue());
		SonarIssueType bugs = buildIssueType(baseURL, key, token, TYPE.BUG);
		SonarIssueType vulnerabilities = buildIssueType(baseURL, key, token, TYPE.VULNERABILITY);
		SonarIssueType codeSmells = buildIssueType(baseURL, key, token, TYPE.CODE_SMELL);
		bean = new SonarMetricBean(prj, bugs, vulnerabilities, codeSmells);
		return bean;
	}
	
	private static SonarIssueType buildIssueType(String baseURL, String key, String token, TYPE type) throws MalformedURLException {
		List<SonarMeasure> measureList = getMeasuresByTypeAndComponent(baseURL, key, token, type.toString());
		return new SonarIssueType.Builder().buildWithMeasures(type.toString(), measureList);
	}

	/**
	 *
	 * @param baseURL
	 * @param key
	 * @param token
	 * @return
	 * @throws MalformedURLException
	 */
	public static JSONObject getAllMeasuresByComponent(String baseURL, String key, String token)
			throws MalformedURLException {
		JSONObject result = new JSONObject();
		for (String type : TYPES) {
			IComponent project = getProjectByKey(baseURL, key, token);
			result.put("component", project.toJson());
			JSONObject ncloc = getLinesOfCodeByComponent(baseURL, key, token).toJson();
			result.put("LINES_OF_CODE", ncloc);
			// [{"MINOR":88},{"MAJOR":7},{"INFO":0},{"CRITICAL":0},{"BLOCKER":0}]
			JSONArray measures = getMeasureBreakdownByTypeAndComponent(baseURL, key, token, type);
			result.put(type, measures);
		}
		return result;
	}

	/**
	 *
	 * @param baseURL
	 * @param key
	 * @param token
	 * @param qualifier
	 * @return
	 * @throws MalformedURLException
	 */
	public static String getCSVReport(String baseURL, String token, String qualifier) throws MalformedURLException {
		StringBuilder report = new StringBuilder();
		JSONArray projectKeys = getProjectKeys(baseURL, token, qualifier);
		report.append(buildLine(ISSUE_TYPE_LIST)).append("\n");
		projectKeys.forEach(prj -> {
			JSONObject component = (JSONObject) prj;
			String line = null;
			try {
				line = getCSVReportLine(baseURL, component.getString("key"), token);
			} catch (MalformedURLException | JSONException e) {
				logger.error("Error while preparing report line", e);
			}
			report.append(line);
		});
		return report.toString();
	}

	/**
	 * 
	 * @param baseURL
	 * @param token
	 * @param qualifier
	 * @return
	 * @throws MalformedURLException
	 */
	public static List<SonarMetricBean> getReport(String baseURL, String token, String qualifier)
			throws MalformedURLException {
		List<String> keys = getProjectKeyList(baseURL, token, qualifier);
		List<SonarMetricBean> report = new ArrayList<>();
		keys.forEach(key -> {
			try {
				report.add(getReportEntry(baseURL, key, token));
			} catch (MalformedURLException e) {
				logger.error(e.getMessage(), e);
			}
		});
		return report;
	}

	/**
	 * 
	 * @param baseURL
	 * @param key
	 * @param token
	 * @return
	 * @throws MalformedURLException
	 */
	protected static String getCSVReportLine(String baseURL, String key, String token) throws MalformedURLException {
		JSONObject jMeasures = getAllMeasuresByComponent(baseURL, key, token);
		StringBuilder allMeasures = new StringBuilder();
		JSONObject project = jMeasures.getJSONObject("component");
		allMeasures.append(project.getString("name")).append(",").append(project.getString("qualifier"));
		Integer ncloc = jMeasures.getJSONObject("LINES_OF_CODE").getInt("ncloc");
		allMeasures.append(",").append(ncloc);
		for (String type : TYPES) {
			String jsontype = jMeasures.getJSONArray(type).toString();
			List<Map<String, Integer>> list = JsonPath.parse(jsontype).read("$.*");
			list.forEach(item -> allMeasures.append(",").append(item.values().toArray()[0]));
		}
		allMeasures.append("\n");
		return allMeasures.toString();

	}


	/**
	 *
	 * @param baseURL
	 * @param key
	 * @param token
	 * @return
	 * @throws MalformedURLException
	 */
	@SuppressWarnings("unused")
	private static List<String> getAllMeasuresByComponents(String baseURL, String key, String token)
			throws MalformedURLException {
		List<String> result = new ArrayList<>();
		String[] issueTypeList = { "component_key", "component_qualifier", "loc", "bug_blocker", "bug_critical",
				"bug_major", "bug_minor", "bug_info", "vulnerability_blocker", "vulnerability_critical",
				"vulnerability_major", "vulnerability_minor", "vulnerability_info", "maintanibility_blocker",
				"maintanibility_critical", "maintanibility_major", "maintanibility_minor", "maintanibility_info" };
		result.add(0, buildLine(issueTypeList));

		for (String type : TYPES) {
			StringBuilder sbLine = new StringBuilder();
			IComponent project = getProjectByKey(baseURL, key, token);
			JSONArray measures = getMeasureBreakdownByTypeAndComponent(baseURL, key, token, type);
			Integer ncloc = getLinesOfCodeByComponent(baseURL, key, token).getValue();
			sbLine.append(key).append(",").append(project.getQualifier()).append(",").append(type).append(",")
					.append(ncloc).append(",");
			measures.forEach(m -> {
				JSONObject jo = (JSONObject) m;
				Set<String> keys = jo.keySet();
				sbLine.append(jo.getInt((String) (keys.toArray()[0]))).append(",");
			});
			String line = sbLine.toString();
			result.add(line.substring(0, line.length() - 1));
		}
		// [{"MINOR":88},{"MAJOR":7},{"INFO":0},{"CRITICAL":0},{"BLOCKER":0}]
		return result;
	}

	/**
	 *
	 * @param list
	 * @return
	 */
	protected static String buildLine(String[] list) {
		StringBuilder sb = new StringBuilder();
		Arrays.asList(list).forEach(s -> sb.append(s).append(","));
		String result = sb.toString();
		return result.substring(0, result.length() - 1);
	}

}
