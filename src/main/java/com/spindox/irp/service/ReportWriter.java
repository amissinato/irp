package com.spindox.irp.service;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Calendar;

import org.apache.commons.io.Charsets;
import org.apache.commons.io.output.FileWriterWithEncoding;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import com.spindox.irp.model.sonar.SonarMetricBean;

public final class ReportWriter {
	private static final Logger logger = LoggerFactory.getLogger(ReportWriter.class);
	private static String[] columns = { "component_name", "component_qualifier", "key"
			//, "type"
			, "loc", "bug_blocker",
			"bug_critical", "bug_major", "bug_minor", "bug_info", "vulnerability_blocker", "vulnerability_critical",
			"vulnerability_major", "vulnerability_minor", "vulnerability_info", "maintanibility_blocker",
			"maintanibility_critical", "maintanibility_major", "maintanibility_minor", "maintanibility_info" };

	private ReportWriter() {

	}

	/**
	 *
	 * @param content
	 * @param fileName
	 * @param path
	 * @throws JSONException
	 * @throws IOException
	 * @throws CsvRequiredFieldEmptyException
	 * @throws CsvDataTypeMismatchException
	 */
	public static void write(List<SonarMetricBean> content, File file)
			throws IOException, CsvRequiredFieldEmptyException {
		FileWriterWithEncoding fw = new FileWriterWithEncoding(file, Charsets.UTF_8);
		try (BufferedWriter buf = new BufferedWriter(fw)) {
			for(int column=0; column<columns.length; column++) {
				if(column != 0) buf.append(";");
				buf.append(columns[column]);
			}
			buf.append("\n");
			StatefulBeanToCsv<SonarMetricBean> beanToCsv = new StatefulBeanToCsvBuilder<SonarMetricBean>(buf)
					.withSeparator(';').build();
			beanToCsv.write(content);
			buf.flush();
		}catch (Exception e) {
			logger.error(e.getMessage(), e);
		}

	}

	/**
	 *
	 * @return
	 */
	public static String fileSuffixTimeStamp() {
		return String.format("-%s_%s", new SimpleDateFormat("yyyy-MM-dd").format(new Date()),
				Calendar.getInstance().getTimeInMillis());
	}

}
