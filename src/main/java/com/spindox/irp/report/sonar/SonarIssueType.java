package com.spindox.irp.report.sonar;

import java.util.List;
import java.util.Objects;

import com.spindox.irp.model.sonar.SonarMeasure;
import com.spindox.irp.model.sonar.SonarProject;

public class SonarIssueType {
	private String type = null;
	private Integer blockerIssues = null;
	private Integer criticalIssues = null;
	private Integer majorIssues = null;
	private Integer minorIssues = null;
	private Integer infoIssues = null;

	protected SonarIssueType(String type, Integer blockerIssues, Integer criticalIssues, Integer majorIssues,
			Integer minorIssues, Integer infoIssues) {
		super();
		this.type = type;
		this.blockerIssues = blockerIssues;
		this.criticalIssues = criticalIssues;
		this.majorIssues = majorIssues;
		this.minorIssues = minorIssues;
		this.infoIssues = infoIssues;
	}

	public static class Builder {
		private String bType = null;
		private Integer bBlockerIssues = null;
		private Integer bCriticalIssues = null;
		private Integer bMajorIssues = null;
		private Integer bMinorIssues = null;
		private Integer bInfoIssues = null;

		public Builder() {
		}

		public Builder(String type) {
			this.bType = type;
		}

		public Builder asType(String type) {
			this.bType = type;
			return this;
		}

		public Builder withBlockerIssues(Integer issues) {
			this.bBlockerIssues = issues;
			return this;
		}

		public Builder withCriticalIssues(Integer issues) {
			this.bCriticalIssues = issues;
			return this;
		}

		public Builder withMajorIssues(Integer issues) {
			this.bMajorIssues = issues;
			return this;
		}

		public Builder withMinorIssues(Integer issues) {
			this.bMinorIssues = issues;
			return this;
		}

		public Builder withInfoIssues(Integer issues) {
			this.bInfoIssues = issues;
			return this;
		}

		/**
		 * This builder method should be used alone: it will overwrite all the other
		 * setters
		 *
		 * @param type
		 * @param measureList
		 * @return
		 */
		public SonarIssueType buildWithMeasures(String type, List<SonarMeasure> measureList) {

			this.bType = type;
			bBlockerIssues = measureList.stream().filter(x -> "BLOCKER".equals(x.getMetric())).findFirst()
					.orElse(new SonarMeasure()).getValue();
			bCriticalIssues = measureList.stream().filter(x -> "CRITICAL".equals(x.getMetric())).findFirst()
					.orElse(new SonarMeasure()).getValue();
			bMajorIssues = measureList.stream().filter(x -> "MAJOR".equals(x.getMetric())).findFirst()
					.orElse(new SonarMeasure()).getValue();
			bMinorIssues = measureList.stream().filter(x -> "MINOR".equals(x.getMetric())).findFirst()
					.orElse(new SonarMeasure()).getValue();
			bInfoIssues = measureList.stream().filter(x -> "INFO".equals(x.getMetric())).findFirst()
					.orElse(new SonarMeasure()).getValue();
			return new SonarIssueType(bType, bBlockerIssues, bCriticalIssues, bMajorIssues, bMinorIssues, bInfoIssues);
		}

		public SonarIssueType build() {
			return new SonarIssueType(bType, bBlockerIssues, bCriticalIssues, bMajorIssues, bMinorIssues, bInfoIssues);
		}
	}

	public String getType() {
		return type;
	}

	public Integer getBlockerIssues() {
		return blockerIssues;
	}

	public Integer getCriticalIssues() {
		return criticalIssues;
	}

	public Integer getMajorIssues() {
		return majorIssues;
	}

	public Integer getMinorIssues() {
		return minorIssues;
	}

	public Integer getInfoIssues() {
		return infoIssues;
	}

	@SuppressWarnings("unused")
	private SonarIssueType() {
	}

	@Override
	public int hashCode() {
		return Objects.hash(type, blockerIssues, criticalIssues, majorIssues, minorIssues, infoIssues);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SonarProject other = (SonarProject) obj;
		return Objects.equals(hashCode(), other.hashCode());
	}

}
