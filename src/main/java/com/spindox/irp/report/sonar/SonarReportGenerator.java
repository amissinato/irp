package com.spindox.irp.report.sonar;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import com.spindox.irp.model.sonar.SonarMetricBean;
import com.spindox.irp.service.ReportWriter;
import com.spindox.irp.service.SonarReport;

public class SonarReportGenerator {
	private static final Logger logger = LoggerFactory.getLogger(SonarReportGenerator.class);
	static final String USAGE = "usage: java -Dlogback.configurationFile=/SonarReport -jar ./irp-jar-with-dependencies.jar baseURL token qualifier filePath";

	public static void main(String[] args) throws CsvRequiredFieldEmptyException {
		String logConfigPath = System.getProperty("logback.configurationFile");
		ClassLoader.getSystemResourceAsStream(logConfigPath);
		String baseURL = args[0];
		try {
			checkURL(baseURL);
		} catch (MalformedURLException e) {
			logger.error("Wrong base URL (should be http(s)://sonar-server:port/api/", e);
			System.exit(400);
		}
		String token = args[1];
		String qualifier = args[2];
		String pathName = args[3];
		Path filePath = null;
		if (token == null || qualifier == null || pathName == null) {
			logger.error("Some parameter(s) was missing");
			System.exit(500);
		}

		filePath = checkPath(pathName);

		List<SonarMetricBean> report = null;
		try {
			report = generateReport(baseURL, token, qualifier);
		} catch (MalformedURLException e) {
			logger.error(e.getMessage());
			System.exit(500);
		}
		try {
			writeToCSV(report, buildFilePathWithSuffix(filePath, ReportWriter.fileSuffixTimeStamp()).toFile());
		} catch (IOException e) {
			logger.error(e.getMessage());
			System.exit(500);
		}

	}

	public static void writeToCSV(List<SonarMetricBean> report, File filePath) throws IOException, CsvRequiredFieldEmptyException {
		ReportWriter.write(report, filePath);

	}

	public static List<SonarMetricBean> generateReport(String baseURL, String token, String qualifier) throws MalformedURLException {
		return SonarReport.getReport(baseURL, token, qualifier);
	}

	/**
	 * Used to avoid unused local variables
	 *
	 * @param ref
	 * @return
	 * @throws MalformedURLException
	 */
	private static URL checkURL(String ref) throws MalformedURLException {
		// Return the URL if the conversion succeeds, otherwise throw back the exception
		return new URL(ref);
	}

	/**
	 * Used to avoid unused local variables
	 *
	 * @param ref
	 * @return
	 * @throws URISyntaxException
	 */
	@SuppressWarnings("unused")
	private static URI checkURI(String ref) throws URISyntaxException {
		// Return the URI if the conversion succeeds, otherwise throw back the exception
		return new URI(ref);
	}

	/**
	 * Used to avoid unused local variables
	 *
	 * @param ref
	 * @return
	 * @throws URISyntaxException
	 */
	private static Path checkPath(String ref) {
		// Return the Path if the conversion succeeds, otherwise throw back the
		// exception
		return Paths.get(ref);
	}

	/**
	 *
	 * @param path
	 * @param suffix
	 * @return
	 */
	private static Path buildFilePathWithSuffix(Path path, String suffix) {
		StringBuilder sb = new StringBuilder();
		String fileName = path.getFileName().toString();
		String[] fileNameParts = fileName.split("\\.");
		String fileWithoutExtension = fileName.substring(0, fileName.lastIndexOf('.'));
		sb.append(fileWithoutExtension).append(suffix).append(".").append(fileNameParts[fileNameParts.length - 1]);
		return Paths.get((path.getParent().toString().concat("\\").concat(sb.toString())));
	}
}
