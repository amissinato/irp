package com.spindox.irp.connector.sonar;

import java.net.MalformedURLException;
import java.net.URL;
import org.apache.commons.lang3.StringUtils;
import org.sonarqube.ws.client.GetRequest;
import org.sonarqube.ws.client.HttpConnector;
import org.sonarqube.ws.client.WsResponse;
public class SonarConnector {
	protected URL serviceURL;
	protected String baseURL;
	protected String queryAPI;
	protected String token;

	/**
	 *
	 * @param serviceURL
	 * @param token
	 * @throws MalformedURLException
	 */
	public SonarConnector(URL serviceURL, String token) throws MalformedURLException {
		if(serviceURL != null) {
		this.serviceURL = serviceURL;
		this.baseURL = StringUtils.substringBeforeLast(serviceURL.toString(), "/");
		this.queryAPI = StringUtils.substringAfterLast(serviceURL.toString(), "/");
		this.token = token;}
		else throw new MalformedURLException("URL parameter cannot be null");
	}

	/**
	 *
	 * @return
	 */
	public final URL getServiceURL() {
		return serviceURL;
	}

	/**
	 *
	 * @param serviceURL
	 */
	public final void setServiceURL(URL serviceURL) {
		this.serviceURL = serviceURL;
	}

	/**
	 *
	 * @return
	 */
	public final String getBaseURL() {
		return baseURL;
	}

	/**
	 *
	 * @param baseURL
	 */
	public final void setBaseURL(String baseURL) {
		this.baseURL = baseURL;
	}

	/**
	 *
	 * @return
	 */
	public final String getQueryAPI() {
		return queryAPI;
	}

	/**
	 *
	 * @param queryAPI
	 */
	public final void setQueryAPI(String queryAPI) {
		this.queryAPI = queryAPI;
	}

	/**
	 *
	 * @return
	 */
	public final String getToken() {
		return token;
	}

	/**
	 *
	 * @param token
	 */
	public final void setToken(String token) {
		this.token = token;
	}

	/**
	 *
	 * @return a @see org.sonarqube.ws.client.WsResponse from the invocation
	 */
	protected WsResponse getHTTPResponse() {
		HttpConnector httpConnector = HttpConnector.newBuilder().url(baseURL).token(token)
				.build();
		WsResponse response = httpConnector.call(new GetRequest(queryAPI));
		response.failIfNotSuccessful();
		return response;
	}

	/**
	 *
	 * @return the String value of the http invocation
	 */
	public String getResponseContent() {
		return getHTTPResponse().content();
	}

}
