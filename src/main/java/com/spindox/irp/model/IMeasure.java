package com.spindox.irp.model;

import java.io.Serializable;

import org.json.JSONObject;

public interface IMeasure extends Serializable {

	public String getMetric();

	public void setMetric(String metric);

	public Integer getValue();

	public void setValue(Integer value);

	public JSONObject toJson();
}