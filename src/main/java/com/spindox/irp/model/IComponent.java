package com.spindox.irp.model;

import org.json.JSONObject;

public interface IComponent {

	public String getOrganization();

	public void setOrganization(String organization);

	public String getId();

	public void setId(String id);

	public String getKey();

	public void setKey(String key);

	public String getName();

	public void setName(String name);

	public String getQualifier();

	public void setQualifier(String qualifier);

	public String getType();

	public void setType(String type);

	public Integer getLinesOfCode();

	public void setLinesOfCode(Integer loc);

	public JSONObject toJson();

}