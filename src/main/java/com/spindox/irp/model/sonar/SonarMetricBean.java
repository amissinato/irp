package com.spindox.irp.model.sonar;

import java.util.Objects;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.json.JSONObject;

import com.opencsv.bean.*;
import com.spindox.irp.model.IComponent;
import com.spindox.irp.report.sonar.SonarIssueType;

public class SonarMetricBean {
	@CsvBindByPosition(position=0)
	private String componentName = null;
	@CsvBindByPosition(position=1)
	private String componentQualifier = null;
	@CsvBindByPosition(position=2)
	private String componentKey = null;
	//skip type
	private String componentType = null;
	@CsvBindByPosition(position=3)
	private Integer linesOfCode = null;
	@CsvBindByPosition(position=4)
	private Integer bugBlocker = null;
	@CsvBindByPosition(position=5)
	private Integer bugCritical = null;
	@CsvBindByPosition(position=6)
	private Integer bugMajor = null;
	@CsvBindByPosition(position=7)
	private Integer bugMinor = null;
	@CsvBindByPosition(position=8)
	private Integer bugInfo = null;
	@CsvBindByPosition(position=9)
	private Integer vulnerabilityBlocker = null;
	@CsvBindByPosition(position=10)
	private Integer vulnerabilityCritical = null;
	@CsvBindByPosition(position=11)
	private Integer vulnerabilityMajor = null;
	@CsvBindByPosition(position=12)
	private Integer vulnerabilityMinor = null;
	@CsvBindByPosition(position=13)
	private Integer vulnerabilityInfo = null;
	@CsvBindByPosition(position=14)
	private Integer codesmellBlocker = null;
	@CsvBindByPosition(position=15)
	private Integer codesmellCritical = null;
	@CsvBindByPosition(position=16)
	private Integer codesmellMajor = null;
	@CsvBindByPosition(position=17)
	private Integer codesmellMinor = null;
	@CsvBindByPosition(position=18)
	private Integer codesmellInfo = null;

	public SonarMetricBean() {
	}

	/**
	 *
	 * @param prj
	 * @param bugs
	 * @param vulnerabilities
	 * @param codeSmells
	 */
	public SonarMetricBean(IComponent prj, SonarIssueType bugs, SonarIssueType vulnerabilities, SonarIssueType codeSmells) {
		super();
		this.componentName = prj.getName();
		this.componentQualifier = prj.getQualifier();
		this.componentKey = prj.getKey();
		this.componentType = prj.getType();
		this.linesOfCode = prj.getLinesOfCode();
		this.bugBlocker = bugs.getBlockerIssues();
		this.bugCritical = bugs.getCriticalIssues();
		this.bugMajor = bugs.getMajorIssues();
		this.bugMinor = bugs.getMinorIssues();
		this.bugInfo = bugs.getInfoIssues();
		this.vulnerabilityBlocker = vulnerabilities.getBlockerIssues();
		this.vulnerabilityCritical = vulnerabilities.getCriticalIssues();
		this.vulnerabilityMajor = vulnerabilities.getMajorIssues();
		this.vulnerabilityMinor = vulnerabilities.getMinorIssues();
		this.vulnerabilityInfo = vulnerabilities.getInfoIssues();
		this.codesmellBlocker = codeSmells.getBlockerIssues();
		this.codesmellCritical = codeSmells.getCriticalIssues();
		this.codesmellMajor = codeSmells.getMajorIssues();
		this.codesmellMinor = codeSmells.getMinorIssues();
		this.codesmellInfo = codeSmells.getInfoIssues();
	}

	/**
	 * @return the componentName
	 */
	public String getComponentName() {
		return componentName;
	}

	/**
	 * @param componentName the componentName to set
	 */
	public void setComponentName(String componentName) {
		this.componentName = componentName;
	}

	/**
	 * @return the componentQualifier
	 */
	public String getComponentQualifier() {
		return componentQualifier;
	}

	/**
	 * @param componentQualifier the componentQualifier to set
	 */
	public void setComponentQualifier(String componentQualifier) {
		this.componentQualifier = componentQualifier;
	}

	/**
	 *
	 * @param componentKey the componentKey to set
	 */
	public void setComponentKey(String componentKey) {
		this.componentKey = componentKey;
	}

	/**
	 *
	 * @return the componentKey
	 */
	public String getComponentKey() {
		return componentKey;
	}

	/**
	 * @return the componentType
	 */
	public String getComponentType() {
		return componentType;
	}

	/**
	 * @param componentType the componentType to set
	 */
	public void setComponentType(String componentType) {
		this.componentType = componentType;
	}

	/**
	 * @return the linesOfCode
	 */
	public Integer getLinesOfCode() {
		return linesOfCode;
	}

	/**
	 * @param linesOfCode the linesOfCode to set
	 */
	public void setLinesOfCode(Integer linesOfCode) {
		this.linesOfCode = linesOfCode;
	}

	/**
	 * @return the bugBlocker
	 */
	public Integer getBugBlocker() {
		return bugBlocker;
	}

	/**
	 * @param bugBlocker the bugBlocker to set
	 */
	public void setBugBlocker(Integer bugBlocker) {
		this.bugBlocker = bugBlocker;
	}

	/**
	 * @return the bugCritical
	 */
	public Integer getBugCritical() {
		return bugCritical;
	}

	/**
	 * @param bugCritical the bugCritical to set
	 */
	public void setBugCritical(Integer bugCritical) {
		this.bugCritical = bugCritical;
	}

	/**
	 * @return the bugMajor
	 */
	public Integer getBugMajor() {
		return bugMajor;
	}

	/**
	 * @param bugMajor the bugMajor to set
	 */
	public void setBugMajor(Integer bugMajor) {
		this.bugMajor = bugMajor;
	}

	/**
	 * @return the bugMinor
	 */
	public Integer getBugMinor() {
		return bugMinor;
	}

	/**
	 * @param bugMinor the bugMinor to set
	 */
	public void setBugMinor(Integer bugMinor) {
		this.bugMinor = bugMinor;
	}

	/**
	 * @return the bugInfo
	 */
	public Integer getBugInfo() {
		return bugInfo;
	}

	/**
	 * @param bugInfo the bugInfo to set
	 */
	public void setBugInfo(Integer bugInfo) {
		this.bugInfo = bugInfo;
	}

	/**
	 * @return the vulnerabilityBlocker
	 */
	public Integer getVulnerabilityBlocker() {
		return vulnerabilityBlocker;
	}

	/**
	 * @param vulnerabilityBlocker the vulnerabilityBlocker to set
	 */
	public void setVulnerabilityBlocker(Integer vulnerabilityBlocker) {
		this.vulnerabilityBlocker = vulnerabilityBlocker;
	}

	/**
	 * @return the vulnerabilityCritical
	 */
	public Integer getVulnerabilityCritical() {
		return vulnerabilityCritical;
	}

	/**
	 * @param vulnerabilityCritical the vulnerabilityCritical to set
	 */
	public void setVulnerabilityCritical(Integer vulnerabilityCritical) {
		this.vulnerabilityCritical = vulnerabilityCritical;
	}

	/**
	 * @return the vulnerabilityMajor
	 */
	public Integer getVulnerabilityMajor() {
		return vulnerabilityMajor;
	}

	/**
	 * @param vulnerabilityMajor the vulnerabilityMajor to set
	 */
	public void setVulnerabilityMajor(Integer vulnerabilityMajor) {
		this.vulnerabilityMajor = vulnerabilityMajor;
	}

	/**
	 * @return the vulnerabilityMinor
	 */
	public Integer getVulnerabilityMinor() {
		return vulnerabilityMinor;
	}

	/**
	 * @param vulnerabilityMinor the vulnerabilityMinor to set
	 */
	public void setVulnerabilityMinor(Integer vulnerabilityMinor) {
		this.vulnerabilityMinor = vulnerabilityMinor;
	}

	/**
	 * @return the vulnerabilityInfo
	 */
	public Integer getVulnerabilityInfo() {
		return vulnerabilityInfo;
	}

	/**
	 * @param vulnerabilityInfo the vulnerabilityInfo to set
	 */
	public void setVulnerabilityInfo(Integer vulnerabilityInfo) {
		this.vulnerabilityInfo = vulnerabilityInfo;
	}

	/**
	 * @return the codesmellBlocker
	 */
	public Integer getCodesmellBlocker() {
		return codesmellBlocker;
	}

	/**
	 * @param codesmellBlocker the codesmellBlocker to set
	 */
	public void setCodesmellBlocker(Integer codesmellBlocker) {
		this.codesmellBlocker = codesmellBlocker;
	}

	/**
	 * @return the codesmellCritical
	 */
	public Integer getCodesmellCritical() {
		return codesmellCritical;
	}

	/**
	 * @param codesmellCritical the codesmellCritical to set
	 */
	public void setCodesmellCritical(Integer codesmellCritical) {
		this.codesmellCritical = codesmellCritical;
	}

	/**
	 * @return the codesmellMajor
	 */
	public Integer getCodesmellMajor() {
		return codesmellMajor;
	}

	/**
	 * @param codesmellMajor the codesmellMajor to set
	 */
	public void setCodesmellMajor(Integer codesmellMajor) {
		this.codesmellMajor = codesmellMajor;
	}

	/**
	 * @return the codesmellMinor
	 */
	public Integer getCodesmellMinor() {
		return codesmellMinor;
	}

	/**
	 * @param codesmellMinor the codesmellMinor to set
	 */
	public void setCodesmellMinor(Integer codesmellMinor) {
		this.codesmellMinor = codesmellMinor;
	}

	/**
	 * @return the codesmellInfo
	 */
	public Integer getCodesmellInfo() {
		return codesmellInfo;
	}

	/**
	 * @param codesmellInfo the codesmellInfo to set
	 */
	public void setCodesmellInfo(Integer codesmellInfo) {
		this.codesmellInfo = codesmellInfo;
	}

	@Override
	public int hashCode() {
		return Objects.hash(componentName, componentQualifier);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (! (obj instanceof SonarMetricBean))
			return false;
		SonarMetricBean other = (SonarMetricBean) obj;
		return  new EqualsBuilder()
				.append(componentName, other.componentName)
				.append(componentQualifier, other.componentQualifier)
				.isEquals();

	}


	@Override
	public String toString() {
		return String.format(
				"SonarMetricBean ["
				+ "component_name=%s, component_qualifier=%s, component_key=%s"
				+ ", component_type=%s, loc=%s"
				+ ", bug_blocker=%s, bug_critical=%s, bug_major=%s, bug_minor=%s, bug_info=%s, vulnerability_blocker=%s, vulnerability_critical=%s, vulnerability_major=%s, vulnerability_minor=%s, vulnerability_info=%s, codesmell_blocker=%s, codesmell_critical=%s, codesmell_major=%s, codesmell_minor=%s, codesmell_info=%s]",
				componentName,  componentQualifier, componentKey, componentType, linesOfCode, bugBlocker, bugCritical, bugMajor,
				bugMinor, bugInfo, vulnerabilityBlocker, vulnerabilityCritical, vulnerabilityMajor,
				vulnerabilityMinor, vulnerabilityInfo, codesmellBlocker, codesmellCritical, codesmellMajor,
				codesmellMinor, codesmellInfo);
	}


	public JSONObject toJson() {
		return new JSONObject().put("component_name", componentName)
				.put("component_qualifier", componentQualifier)
				.put("component_key", componentKey)
				.put("component_type", componentType)
				.put("loc", linesOfCode)
				.put("bug_blocker", bugBlocker)
				.put("bug_critical", bugCritical)
				.put("bug_major", bugMajor)
				.put("bug_minor", bugMinor)
				.put("bug_info", bugInfo)
				.put("vulnerability_blocker", vulnerabilityBlocker)
				.put("vulnerability_critical", vulnerabilityCritical)
				.put("vulnerability_major", vulnerabilityMajor)
				.put("vulnerability_minor", vulnerabilityMinor)
				.put("vulnerability_info", vulnerabilityInfo)
				.put("maintenability_blocker", codesmellBlocker)
				.put("maintenability_critical", codesmellCritical)
				.put("maintenability_major", codesmellMajor)
				.put("maintenability_minor", codesmellMinor)
				.put("maintenability_info", codesmellInfo);

	}

}
