package com.spindox.irp.model.sonar;

import java.util.Objects;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.json.JSONObject;

import com.spindox.irp.model.IMeasure;

public class SonarMeasure implements IMeasure {


	/**
	 *
	 */
	private static final long serialVersionUID = -5858603772381759967L;
	protected String metric = null;
	protected Integer value = null;

	/**
	 * @param metric
	 * @param value
	 */
	public SonarMeasure(String metric, Integer value) {
		this.metric = metric;
		this.value = value;
	}

	public SonarMeasure() {
		this(null, null);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.spindox.irp.model.sonar.IMeasure#getMetric()
	 */
	@Override
	public final String getMetric() {
		return metric;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.spindox.irp.model.sonar.IMeasure#setMetric(java.lang.String)
	 */
	@Override
	public final void setMetric(String metric) {
		this.metric = metric;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.spindox.irp.model.sonar.IMeasure#getValue()
	 */
	@Override
	public final Integer getValue() {
		return value;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.spindox.irp.model.sonar.IMeasure#setValue(java.lang.Integer)
	 */
	@Override
	public final void setValue(Integer value) {
		this.value = value;
	}

	@Override
	public int hashCode() {
		return Objects.hash(metric, value);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (! (obj instanceof SonarMeasure))
			return false;
		SonarMeasure other = (SonarMeasure) obj;
		return  new EqualsBuilder()
				.append(metric, other.metric)
				.append(value, other.value)
				.isEquals();
	}

	@Override
	public String toString() {
		return String.format("metric=%s, value=%s", metric, value);
	}

	public JSONObject toJson() {
		return new JSONObject().put(getMetric(), getValue());
	}

}
