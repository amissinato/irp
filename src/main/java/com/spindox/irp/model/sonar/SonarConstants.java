/**
 * 
 */
package com.spindox.irp.model.sonar;

/**
 * @author andrea
 *
 */
public final class SonarConstants {
	public static final String PARAM_COMPONENTNAME = "nm";
	public static final String PARAM_COMPONENTKEY = "k";
	public static final String PARAM_COMPONENTTYPE = "sc";
	public static final String PARAM_COMPONENTQUALIFIER = "qu";
	public static final String PARAM_COMPONENTID = "id";
	
	private SonarConstants() {}

}
