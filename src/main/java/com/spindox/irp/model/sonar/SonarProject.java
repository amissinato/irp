package com.spindox.irp.model.sonar;

import java.util.Objects;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.json.JSONObject;

import com.spindox.irp.model.IComponent;

public class SonarProject implements IComponent {
	protected static final int DEFAULT_INDENTATION = 3;
	static final long serialVersionUID = -8107964066682985707L;
	protected String organization = null; // e.g.: "default-organization",
	protected String id = null; // e.g.: "AWc1o5kAqZ8k5FHnC85i"
	protected String key = null; // e.g.: "com.spindox.tim.cartmanager.backend.data-gateway"
	protected String name = null; // e.g.: "Cart Manager Business: data-gateway"
	protected String qualifier = null; // e.g.: "TRK"
	protected String type = null; // e.g.: "PRJ"
	protected Integer linesOfCode;

	/**
	 * @param organization
	 * @param id
	 * @param key
	 * @param name
	 * @param qualifier
	 */
	public SonarProject(String organization, String id, String key, String name, String qualifier, String type,
			Integer linesOfCode) {
		super();
		this.organization = organization;
		this.id = id;
		this.key = key;
		this.name = name;
		this.qualifier = qualifier;
		this.type = type;
		this.linesOfCode = linesOfCode;

	}

	/**
	 * @param organization
	 * @param id
	 * @param key
	 * @param name
	 * @param qualifier
	 */
	public SonarProject(String key, String qualifier, String name) {
		this(null, null, key, name, qualifier, null, null);
	}

	/**
	 *
	 */
	public SonarProject() {

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.spindox.irp.model.sonar.IComponent#getOrganization()
	 */
	@Override
	public String getOrganization() {
		return organization;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.spindox.irp.model.sonar.IComponent#setOrganization(java.lang.String)
	 */
	@Override
	public void setOrganization(String organization) {
		this.organization = organization;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.spindox.irp.model.sonar.IComponent#getId()
	 */
	@Override
	public String getId() {
		return id;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.spindox.irp.model.sonar.IComponent#setId(java.lang.String)
	 */
	@Override
	public void setId(String id) {
		this.id = id;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.spindox.irp.model.sonar.IComponent#getKey()
	 */
	@Override
	public String getKey() {
		return key;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.spindox.irp.model.sonar.IComponent#setKey(java.lang.String)
	 */
	@Override
	public void setKey(String key) {
		this.key = key;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.spindox.irp.model.sonar.IComponent#getName()
	 */
	@Override
	public String getName() {
		return name;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.spindox.irp.model.sonar.IComponent#setName(java.lang.String)
	 */
	@Override
	public void setName(String name) {
		this.name = name;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.spindox.irp.model.sonar.IComponent#getQualifier()
	 */
	@Override
	public String getQualifier() {
		return qualifier;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.spindox.irp.model.sonar.IComponent#setQualifier(java.lang.String)
	 */
	@Override
	public void setQualifier(String qualifier) {
		this.qualifier = qualifier;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.spindox.irp.model.sonar.IComponent#getType()
	 */
	@Override
	public String getType() {
		return type;
	}

	public Integer getLinesOfCode() {
		return linesOfCode;
	}

	public void setLinesOfCode(Integer linesOfCode) {
		this.linesOfCode = linesOfCode;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.spindox.irp.model.sonar.IComponent#setType(java.lang.String)
	 */
	@Override
	public void setType(String type) {
		this.type = type;
	}

	@Override
	public int hashCode() {
		return Objects.hash(key, id, organization, name, qualifier, type, linesOfCode);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof SonarProject))
			return false;
		SonarProject other = (SonarProject) obj;
		return new EqualsBuilder().append("id", other.id).append(key, other.key)
				.append(organization, other.organization).append(name, other.name).append(qualifier, other.qualifier)
				.append(type, other.type).append(linesOfCode, other.linesOfCode).isEquals();
	}


	public JSONObject toJson() {
		JSONObject jo = new JSONObject();
		jo.put("id", getId()).put("key", getKey()).put("organization", getOrganization())
			.put("name", getName()).put("qualifier", getQualifier()).put("linesOfCode", getLinesOfCode());
		return jo;
	}

	public String toJSONString(int indentation) {
		return toJson().toString(indentation);
	}

	/**
	 *
	 * @return the JSON string formatted with the {@see #DEFAULT_INDENTATION} indentation
	 */
	public String toJSONString() {
		return toJSONString(DEFAULT_INDENTATION);
	}
	public String toString() {
		return String.format(
				"SonarProject [organization=%s, id=%s, key=%s, name=%s, qualifier=%s, type=%s, linesOfCode=%s]",
				organization, id, key, name, qualifier, type, linesOfCode);
	}

}
